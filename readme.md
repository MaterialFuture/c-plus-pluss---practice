# C++ Practice

This project is for practicing syntax in C++ and learning conventions.

I'm still learning both best practice and most effecient way to use my IDEs (Eclipse/Kdevelop)

### Contents 
- The `exercises/` directory that's where all of the exercises I'm working on from my C++ course will be.
- The `challenges/` directory will be challenges from a code challenge that I will tackle to get myself to better understand the language by making me create simple and complex things.
- The `notes/` directory will be any notes on any manner of thing pertaining to C/C++.