#include <iostream>
#include "utils.h"

using namespace std;

void showMenu(){
	string selections[2] = { //Specifying the length of the array for memory reasons
		"Fahrenheit",
		"Celsius",
	};
	int selec_length = sizeof(selections)/sizeof(selections[0]); //Get length of array, making a variable so it's reusable, if I need it.
	for (unsigned int a = 0; a < selec_length; a = a + 1) { //List all values based on length, could probably so the as a function so the do/while is cleaner.
		cout << (a+1) << ". \t" << selections[a] << endl;
	}
}

int userSelection() {
	string selections[2] = { //Specifying the length of the array for memory reasons
		"Fahrenheit",
		"Celsius",
	};

	int user_answer;
	do {
		showMenu();

		cout << "Please Select One..." << endl;
		cin >> user_answer;

		if (!user_answer)
			cout << "Not a valid selection. Try again..." << endl;
	} while(!user_answer); //Do/While is chosen for the reason for user input, so they don't have to rerun to try again

	return user_answer;
}

void showAnswer() {
	string selections[2] = { //Specifying the length of the array for memory reasons
		"Fahrenheit",
		"Celsius",
	};
	int user_answer = userSelection();

	double user_temp; //user input as to what they want the temp to be
	switch(user_answer) { //Switch case is better for memory reasons, must be a better way of displaying the output though, maybe store ina variable.
		case 1:
			cout << selections[0] << endl;
			cout << "What temperature? :" << flush;

			cin >> user_temp;
			cout << convertToFahrenheit(user_temp);

			break;
		case 2:
			cout << selections[1] << endl;
			cout << "What temperature? :" << flush;

			cin >> user_temp;
			cout << convertToCelsius(user_temp);

			break;
		}
}

int main() {
	showAnswer();
  return 0;
}

/*	Notes:
 * 	The way the doubles handle decimals is dumb, so I'll need to set it to be more accurate.
 * 	Bonus for myself would be to have a conversion for Kelvin from C and F.
 * 	I believe good practice is to have functions like the conversions into separate .h files, but I'll look more into that.
 * 	I know the length of the array, however for demonstration purposes I have the selec_length var to find that out.
 *
 * 	Update: broke out the conversions into the utils.h for practice. I also broke down the components of the program into functions to make it easier to read.
 */
