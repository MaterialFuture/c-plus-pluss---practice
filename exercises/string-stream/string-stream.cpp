#include <iostream>
#include <sstream>
using namespace std;

int main(){
  string name = "Bob";
  int age = 32;

  stringstream ss;

  ss << "Name: ";
  ss << name;
  ss << ", Age: ";
  ss << age;

  string info = ss.str();
  cout << info << endl;

  /*
   * Normally you would just do a + operator but strings dont work that way in C, so you need to
   * import <sstream> and then send each line into std::ss, then convert that stream to a string and std::cout
   */

  //  string info = "Name: " + name + ", Age: " + age;
	//  cout << info;

  return 0;
}
