#ifndef CAT_H_
#define CAT_H_

class Cat {
private:
	bool happy;
public:
	void pet();
	void meow();
	Cat();
	~Cat();
};

#endif /* CAT_H_ */
