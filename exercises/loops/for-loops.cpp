#include <iostream>
using namespace std;

int countToSix() {
  cout << "For Loop" << endl;
  for (int i=0; i<6; i++) {
    cout << i << endl;
  }
	return 0;
}

int countBreak() {
	cout << "Breaking For Loop" << endl;
	for (int i=0; i<6; i++) {
		cout << i << endl;
		if(i==3)
		  cout << "I'm breaking out" << endl;
		  break;
	}
	return 0;
}

int forLoopContinue() {
	cout << "Checking Passwords..." << endl;
	for (int i=0; i<6; i++) { //This sort of loop would be useful for checking a large list of items like in an array to see if values are valid
	  cout << i << " - Checking Password" << endl;
	  if(i==3)
	    cout << i << " - Password Failed!!" << endl;
	    continue;
	  cout << "Checking Passwords" << endl;
	}
	return 0;
}


int main(){
  cout << countToSix() << endl;
  cout << countBreak() << endl;
  cout << forLoopContinue() << endl;
	
  return 0;
}
