/*
 * Person.cpp
 *
 *  Created on: Jan 29, 2020
 *      Author: materialfuture
 */

#include "Person.h"
#include <sstream>

//Person::Person() {
//	name 	= "Undefined";
//	age 	= 0;
//}


Person::Person(string newName, int newAge) {
	name 	= newName;
	age 	= newAge;
}

string Person::toString() {
	stringstream ss;

	ss << "Your name: ";
	ss << name;
	ss << ", Your Age: ";
	ss << age;

	string info = ss.str();

	return info;
}

