//============================================================================
// Name        : constructor-overload.cpp
// Author      : MaterialFuture
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

#include "Person.h"
using namespace std;

int main() {

	Person person("Bill", 28); 					//Accept data
	cout << person.toString() << endl;	//Print info of person to string

	return 0;
}
