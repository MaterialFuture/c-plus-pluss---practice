#include <iostream>
#include "Cat.h"
using namespace std;

void Cat::meow() {
	if(happy)
		cout << "Meow" << endl;
}

void Cat::pet() {
	cout << "You pet the cat..." << endl;
	happy = 1;
}
