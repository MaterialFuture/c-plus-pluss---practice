#ifndef CAR_H_
#define CAR_H_

class Car {
private:
	bool insurance;
	bool car_started;
	bool in_park = 0;

public:
	bool driving;
	void getInsurance();
	void startCar();
	void drive();
};

#endif /* CAR_H_ */
