#include <iostream>
#include "Cat.h"
#include "Car.h"

using namespace std;

int main() {

	Cat bob;
	bob.meow(); // Will Not Meow
	bob.pet(); 	// Makes Happy
	bob.meow(); // Will Meow

	cout << endl;

	Car mazda_crx;
	mazda_crx.getInsurance();
	mazda_crx.startCar();
	mazda_crx.drive();

	return 0;
}
