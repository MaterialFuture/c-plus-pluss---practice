#include "Person.h"
#include <iostream>
using namespace std;

Person::Person() {
	name = "Anon"; //Default Name
}

string Person::toString(){
	return "Your default name: " + name;
}

void Person::setName(string newName){
	name = newName;
}

string Person::getName() {
	return name;
}
