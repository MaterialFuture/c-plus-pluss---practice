#include <iostream>

#include "Person.h"
using namespace std;


int main() {
	Person player;
	cout << player.toString() << endl; //print default name...

	cout << "Enter new name: " << flush;
	string user_input_name;
	cin >> user_input_name;
	// Put do/white loop to check if name if type string only otherwise loop and return exception
	player.setName(user_input_name);
	cout << "New name of player: " << player.getName() << endl;

	return 0;
}
