#include <iostream>
using namespace std;

//The function just does the math, while main() handle the visuals. Will rework to be used as a real-world app rather than for show in practice.
int main() {
  double tempf;
  cout << "Lets Convert Temp from F -> C..." << endl;

  cout << "Enter the temperature in Fahrenheit:" << endl;
  cin >> tempf;

  double tempc = (tempf - 32) / 1.8;
  cout << "The temp is " << tempc << " degrees Celsius.\n";
  return 0;
}
