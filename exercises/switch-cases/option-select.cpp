#include <iostream>
using namespace std;

int main(){
  string selection[] = {
    "Select Value",
    "Delete Value",
    "List All Values",
    "Search For Values",
    "Quit"
  };
  for (unsigned int a = 0; a < 5; a = a + 1) { //This must not go over the amount that's in the array
    cout << (a+1) << ". \t" << selection[a] << endl;
  } //I'm doing (a+1) so it shows the numbers nicely in the console

  cout << "Enter Selection: " << endl;
  int value;
  cin >> value;

  switch (value) //Switch cases are a better use for this scenario since it's better on memory overall
  {
  case 5:
    cout << "Quitting..." << endl;
    break;
  case 4:
    cout << "Searching for Values..." << endl;
    break;
  case 3:
    cout << "Listing all values..." << endl;
    break;
  case 2:
    cout << "Deleting Value..." << endl;
    break;
  case 1:
    cout << "Selecting Value..." << endl;
    break;
  default:
    cout << "Not sure what you are doing so I'm going to break" << endl;
    break;
  }
  return 0;
}
